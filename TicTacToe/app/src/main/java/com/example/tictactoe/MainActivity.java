package com.example.tictactoe;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    boolean gameActive = true;

    // Player representation
    // 0 - X
    // 1 - O
    int activePlayer = 0;
    int[] gameState = {2, 2, 2, 2, 2, 2, 2, 2, 2};

    // State meanings:
    //    0 - X
    //    1 - O
    //    2 - Null
    // put all win positions in a 2D array
    int[][] winPositions = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8},
            {0, 3, 6}, {1, 4, 7}, {2, 5, 8},
            {0, 4, 8}, {2, 4, 6}};
    public static int counter = 0;

    public void playerTap(View view) {
        ImageView img = (ImageView) view;
        ImageView finalStatus  = findViewById(R.id.finalStatus);
        int tappedImage = Integer.parseInt(img.getTag().toString());

        // In case tapped image is empty
        if (gameState[tappedImage] == 2) {
            // increase the counter
            // after every tap
            counter++;

            // check if its the last box
            if (counter == 9) {
                // reset the game
                gameActive = false;
            }

            // mark this position
            gameState[tappedImage] = activePlayer;

            // this will give a motion
            // effect to the image
            img.setTranslationY(-1000f);

            // change the active player
            if (activePlayer == 0) {
                // set the image of x
                img.setImageResource(R.drawable.x);
                activePlayer = 1;

                // change the status
                finalStatus.setImageResource(R.drawable.oplay);
            } else {
                // set the image of o
                img.setImageResource(R.drawable.o);
                activePlayer = 0;

                // change the status
                finalStatus.setImageResource(R.drawable.xplay);

            }
            img.animate().translationYBy(1000f).setDuration(300);
        }

        int flag = 0;
        // Check if any player has won
        for (int index=0; index < winPositions.length; index++) {
            int[] winPosition = winPositions[index];
            if (gameState[winPosition[0]] == gameState[winPosition[1]] &&
                    gameState[winPosition[1]] == gameState[winPosition[2]] &&
                    gameState[winPosition[0]] != 2) {

                flag = 1;
                String winnerStr;

                // game reset function be called
                gameActive = false;
                if (gameState[winPosition[0]] == 0) {
                    winnerStr = "X has won";
                    finalStatus.setImageResource(R.drawable.xwin);
                } else {
                    winnerStr = "O has won";
                    finalStatus.setImageResource(R.drawable.owin);
                }

                // Place the image of the winning move on the board
                ImageView ResultPic  = findViewById(R.id.resultPic);
                String pictureName = "mark" + index;
                String uri = "@drawable/" + pictureName;
                int imageResource = getResources().getIdentifier(uri, null,getPackageName());
                Drawable res = getResources().getDrawable(imageResource);
                ResultPic.setTranslationY(-1000);
                ResultPic.animate().translationYBy(1000f).setDuration(300);
                ResultPic.setImageDrawable(res);
            }
        }

        // set the status if the match draw
        if (counter >= 9 && flag == 0) {
            finalStatus.setImageResource(R.drawable.nowin);
        }

        // Pop the the play again button if the game is over
        if (!gameActive) {
            Button tryAgain  = findViewById(R.id.playAgain);
            tryAgain.setVisibility(View.VISIBLE);
        }
    }

    // reset the game
    public void gameReset(View view) {
        gameActive = true;
        activePlayer = 0;
        for (int i = 0; i < gameState.length; i++) {
            gameState[i] = 2;
        }

        // remove all the images from the boxes inside the grid
        ((ImageView) findViewById(R.id.imageView0)).setImageResource(0);
        ((ImageView) findViewById(R.id.imageView1)).setImageResource(0);
        ((ImageView) findViewById(R.id.imageView2)).setImageResource(0);
        ((ImageView) findViewById(R.id.imageView3)).setImageResource(0);
        ((ImageView) findViewById(R.id.imageView4)).setImageResource(0);
        ((ImageView) findViewById(R.id.imageView5)).setImageResource(0);
        ((ImageView) findViewById(R.id.imageView6)).setImageResource(0);
        ((ImageView) findViewById(R.id.imageView7)).setImageResource(0);
        ((ImageView) findViewById(R.id.imageView8)).setImageResource(0);
        ((ImageView) findViewById(R.id.finalStatus)).setImageResource(0);
        ((ImageView) findViewById(R.id.resultPic)).setImageResource(0);
        counter = 0;

        Button tryAgain  = findViewById(R.id.playAgain);
        tryAgain.setVisibility(View.INVISIBLE);

        ImageView finalStatus  = findViewById(R.id.finalStatus);
        finalStatus.setImageResource(R.drawable.xplay);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}